package com.cuccorese.desper.service;

import com.cuccorese.desper.exceptions.ApiException;
import com.cuccorese.desper.web.response.HotelResponse;
import com.cuccorese.desper.web.response.LikeHotelResponse;
import java.util.List;

/**
 * Interface del servicio para la logica del negocio.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public interface HotelService {

    /**
     * Guarda un snashot de hoteles.
     *
     * @param catidad cantidad de hoteles a guardar.
     */
    void saveSnapshot(Integer catidad);

    /**
     * Obtiene los hoteles y precios de la ciudad.
     *
     * @param cityId el id de la ciudad.
     * @return la lista de hoteles con sus precios.
     */
    List<HotelResponse> getHotelsAndPrices(String cityId);

    /**
     * Suma un punto de "me gusta" al hotel.
     *
     * @param hotelId el id del hotel.
     * @param like gusta o no gusta.
     * @return el id y la cantidad de "me gustas"
     * @throws com.cuccorese.desper.exceptions.ApiException
     */
    LikeHotelResponse iLikeHotel(String hotelId, Boolean like) throws ApiException;

}
