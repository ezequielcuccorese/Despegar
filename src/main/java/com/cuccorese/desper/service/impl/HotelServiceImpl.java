package com.cuccorese.desper.service.impl;

import com.cuccorese.desper.DesperApplication;
import com.cuccorese.desper.config.GlobalSettings;
import com.cuccorese.desper.domain.Hotel;
import com.cuccorese.desper.domain.HotelWrapper;
import com.cuccorese.desper.domain.LikeDislikeHotel;
import com.cuccorese.desper.domain.PriceWraper;
import com.cuccorese.desper.exceptions.ApiException;
import com.cuccorese.desper.exceptions.HotelInexistentException;
import com.cuccorese.desper.repository.HotelRepository;
import com.cuccorese.desper.repository.LikeRepository;
import com.cuccorese.desper.service.HotelService;
import com.cuccorese.desper.util.menssages.GeneralMenssages;
import com.cuccorese.desper.util.menssages.Messages;
import com.cuccorese.desper.web.response.HotelResponse;
import com.cuccorese.desper.web.response.LikeHotelResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Servicio principal con la logica de negocio.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Service
public class HotelServiceImpl implements HotelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DesperApplication.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Messages messages;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private LikeRepository likeRepository;

    @Value("${api.despegar.host}")
    private String urlHost;

    @Value("${api.despegar.hotels}")
    private String urlHotel;

    @Value("${api.despegar.prices}")
    private String urlPrices;

    @Override
    public void saveSnapshot(Integer catidad) {
        LOGGER.info(messages.get(GeneralMenssages.LOGINFOSNAPSHOT));
        hotelRepository.save(Arrays.asList(findHotels(catidad).getItems()));
        LOGGER.info(messages.get(GeneralMenssages.LOGINFOGUARDADOEXITOSO));

    }

    @Override
    //En caso de lanzar una excpecion el spring reintenta buscar hasta 2 veces, con 5000 segundos de espera.
    @Retryable(value = {RuntimeException.class, Exception.class}, maxAttempts = 2, backoff = @Backoff(delay = 5000))
    public List<HotelResponse> getHotelsAndPrices(String cityId) {
        List<Hotel> hoteles = hotelRepository.findByCityId(cityId);

        String hotelesIds = hoteles.stream()
                .map((hotel) -> hotel.getHotelId()).collect(Collectors.joining(","));

        List<HotelResponse> hotelesDTO = Arrays.asList(this.findPrices(hotelesIds).getItems());

        hotelesDTO = hotelesDTO.stream().filter(hotel -> hotel.getPrice() != null)
                .collect(Collectors.toList());

        hotelesDTO.forEach(
                (hotelDTO) -> {
                    Hotel hotel = hoteles.stream()
                            .filter(o -> o.getHotelId().equals(hotelDTO.getHotelId()))
                            .findFirst().get();

                    hotelDTO.setStarts(hotel.getStars());
                    hotelDTO.setMainPicture(hotel.getMainPicture());
                    hotelDTO.setName(hotel.getName());
                });

        return hotelesDTO;
    }

    @Override
    public LikeHotelResponse iLikeHotel(String hotelId, Boolean like) throws ApiException {

        if (hotelRepository.findOne(hotelId) != null) {
            LikeDislikeHotel likeDislikeHotel = likeRepository.findByHotelId(hotelId);
            if (likeDislikeHotel == null) {
                likeDislikeHotel = new LikeDislikeHotel();
                likeDislikeHotel.setHotelId(hotelId);
            }
            likeDislikeHotel.setLikes(like ? likeDislikeHotel.getLikes() + 1 : likeDislikeHotel.getLikes() - 1);

            LikeHotelResponse hotelResponse = new LikeHotelResponse();
            hotelResponse.setId(hotelId);

            if (likeDislikeHotel.getLikes() >= 0) {
                hotelResponse.setLike(likeDislikeHotel.getLikes());
            } else {
                hotelResponse.setDislike(likeDislikeHotel.getLikes() * -1);
            }
            likeRepository.save(likeDislikeHotel);
            return hotelResponse;
        }
        throw new HotelInexistentException();
    }

    /**
     * Busca en el servicio de despegar una cantidad de hoteles dados.
     *
     * @param catidad la cantidad de hoteles.
     * @return el wrapper del servicio.
     */
    private HotelWrapper findHotels(Integer catidad) {
        LOGGER.info(messages.get(GeneralMenssages.LOGINFOCONTACTANDOALSERVICIO));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlHost + urlHotel)
                .queryParam(GlobalSettings.OFFSET, GlobalSettings.OFFSETAMOUNT)
                .queryParam(GlobalSettings.LIMIT, catidad);
        return restTemplate.getForObject(builder.build().encode().toUri(), HotelWrapper.class);

    }

    /**
     * Busca los precios de los hotoles.
     *
     * @param hotels los id de los hoteles.
     * @return el wrappers de los precios.
     */
    private PriceWraper findPrices(String hotels) {
        LOGGER.info(messages.get(GeneralMenssages.LOGINFOCONTACTANDOALSERVICIO));

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlHost + urlHotel + urlPrices)
                .queryParam(GlobalSettings.HOTELS, hotels)
                .queryParam(GlobalSettings.COUNTRY, GlobalSettings.AR)
                .queryParam(GlobalSettings.DISTRIBUTION, GlobalSettings.DOS)
                .queryParam(GlobalSettings.INCLUDE, GlobalSettings.CHEAPEST);

        return restTemplate.getForObject(builder.build().encode().toUri(), PriceWraper.class);
    }

    @Recover
    public List<HotelResponse> recover(RuntimeException e) {
        LOGGER.error(messages.get(GeneralMenssages.LOGERRORCONTACTANDOALSERVICIO), e);

        return new ArrayList<>();
    }
}
