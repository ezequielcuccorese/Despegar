package com.cuccorese.desper.web.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"hotelId"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotelResponse {

    private String hotelId;
    private String name;
    private String starts;
    @JsonProperty("main_picture")
    private String mainPicture;
    @JsonProperty("price_detail")
    private Response price;

    @JsonProperty(value = "id", access = JsonProperty.Access.READ_ONLY)
    public String getHotelId() {
        return hotelId;
    }

    @JsonProperty(value = "hotel_id", access = JsonProperty.Access.WRITE_ONLY)
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }
}
