/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.desper.web.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class LikeHotelResponse {

    private String id;
    private Integer like;
    private Integer dislike;
}
