package com.cuccorese.desper.web;

import com.cuccorese.desper.exceptions.ApiException;
import com.cuccorese.desper.service.HotelService;
import com.cuccorese.desper.web.response.HotelResponse;
import com.cuccorese.desper.web.response.LikeHotelResponse;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador para publicar un servicio REST.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@RestController
@RequestMapping(value = "/destinder/hotels")
public class HotelControllers {

    @Autowired
    private HotelService hotelService;

    @ResponseBody
    @RequestMapping(path = "/prices", method = RequestMethod.GET)
    public List<HotelResponse> getHotel(@RequestParam("city_id") String cityId) {

        return hotelService.getHotelsAndPrices(cityId);
    }

    @ResponseBody
    @RequestMapping(path = "/likes", method = RequestMethod.POST)
    public LikeHotelResponse postLike(@RequestBody HotelResponse hotel) throws ApiException {
        return hotelService.iLikeHotel(hotel.getHotelId(), Boolean.TRUE);
    }

    @ResponseBody
    @RequestMapping(path = "/likes", method = RequestMethod.DELETE)
    public LikeHotelResponse deleteLike(@RequestBody HotelResponse hotel) throws ApiException {
        return hotelService.iLikeHotel(hotel.getHotelId(), Boolean.FALSE);
    }

}
