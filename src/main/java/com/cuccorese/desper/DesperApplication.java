package com.cuccorese.desper;

import com.cuccorese.desper.service.HotelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.annotation.EnableRetry;

/**
 * Entry point de la aplicacion para iniciar.
 *
 * @author Ezequiel Cuccorese
 */
@SpringBootApplication
@EnableRetry
public class DesperApplication {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DesperApplication.class);
    
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(DesperApplication.class, args);
    }

    /**
     * Se implementa un commandLineRunner para hacer la importacion de datos del
     * api rest.
     *
     * @param service
     * @return
     */
    @Bean
    public CommandLineRunner demo(HotelService service) {
        return (args) -> {
            Integer catidad = 1000;
            LOGGER.info("Buscando hoteles en el servico.");
            service.saveSnapshot(catidad);
        };
    }
}
