package com.cuccorese.desper.domain;

import lombok.Data;

/**
 * Modelo de datos para wrapear el json del servicio.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
public class HotelWrapper {

    private Hotel[] items;
}
