package com.cuccorese.desper.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

/**
 * Modelo de datos para los like de los hoteles
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"hotelId"})
@JsonIgnoreProperties(ignoreUnknown = true)
@RedisHash("likeDislikeHotel")
public class LikeDislikeHotel {

    @Id
    @Indexed
    private String hotelId;
    private Integer likes = 0;
}
