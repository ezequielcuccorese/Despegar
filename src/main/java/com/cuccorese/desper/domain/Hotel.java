package com.cuccorese.desper.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Map;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

/**
 * Modelo de dato para los hoteles.
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@NoArgsConstructor
@ToString(exclude = {"hotelId"})
@JsonIgnoreProperties(ignoreUnknown = true)
@RedisHash("hotel")
public class Hotel implements Serializable {

    @Id
    private String hotelId;
    @Indexed
    private String name;
    @Indexed
    private String stars;

    @Indexed
    private String cityId;

    private String mainPicture;

    public Hotel(String hotelId, String name, String stars, String cityId, String mainPicture) {
        this.hotelId = hotelId;
        this.name = name;
        this.stars = stars;
        this.cityId = cityId;
        this.mainPicture = mainPicture;
    }

    @JsonProperty(value = "hotel_id", access = JsonProperty.Access.READ_ONLY)
    public String getHotelId() {
        return hotelId;
    }

    @JsonProperty(value = "id", access = JsonProperty.Access.WRITE_ONLY)
    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    @JsonProperty(value = "nombre", access = JsonProperty.Access.READ_ONLY)
    public String getName() {
        return name;
    }

    @JsonProperty(value = "name", access = JsonProperty.Access.WRITE_ONLY)
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(value = "estrellas", access = JsonProperty.Access.READ_ONLY)
    public String getStars() {
        return stars;
    }

    @JsonProperty(value = "stars", access = JsonProperty.Access.WRITE_ONLY)
    public void setStars(String stars) {
        this.stars = stars;
    }

    @JsonProperty(value = "city_id", access = JsonProperty.Access.READ_ONLY)
    public String getCityId() {
        return cityId;
    }

    @JsonProperty(value = "location", access = JsonProperty.Access.WRITE_ONLY)
    public void setCityId(Map<String, Object> location) {
        this.cityId = ((Map) location.get("city")).get("id").toString();
    }

    @JsonProperty(value = "main_picture", access = JsonProperty.Access.READ_ONLY)
    public String getMainPicture() {
        return mainPicture;
    }

    @JsonProperty(value = "main_picture", access = JsonProperty.Access.WRITE_ONLY)
    public void setMainPicture(Map<String, Object> mainPicture) {
        this.mainPicture = mainPicture.get("url").toString();
    }
}
