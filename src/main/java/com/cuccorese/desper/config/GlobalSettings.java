/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cuccorese.desper.config;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
public class GlobalSettings {

    //Request de hoteles
    public static final int OFFSETAMOUNT = 50000;
    public static final String LIMIT = "limit";
    public static final String OFFSET = "offset";

    //request de precios
    public static final String HOTELS = "hotels";
    public static final String COUNTRY = "country";
    public static final String DISTRIBUTION = "distribution";
    public static final String INCLUDE = "include";
    public static final String AR = "ar";
    public static final String DOS = "2";
    public static final String CHEAPEST = "cheapest";

}
