package com.cuccorese.desper.repository;

import com.cuccorese.desper.DesperApplication;
import com.cuccorese.desper.domain.Hotel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Ezequiel Cuccorese <ezequiel.cuccorese@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DesperApplication.class)
public class HotelRepositoryTest<K, V> {

    /**
     * We need to have a Redis server instance available. <br />
     * 1) Start/Stop an embedded instance or reuse an already running local
     * installation <br />
     * 2) Ignore tests if startup failed and no server running locally.
     */
    public static @ClassRule
    RuleChain rules = RuleChain
            .outerRule(EmbeddedRedisServer.runningAt(6379).suppressExceptions())
            .around(RequiresRedisServer.onLocalhost().atLeast("3.2"));

    /**
     * {@link Charset} for String conversion *
     */
    private static final Charset CHARSET = Charset.forName("UTF-8");

    @Autowired
    private RedisOperations<K, V> operations;

    @Autowired
    private HotelRepository repository;

    /*
	 * Hoteles para testear.
     */
    private final Hotel praia = new Hotel("372914", "Valentina Praia Hotel", "3", "8965", "url1");
    private final Hotel marbella = new Hotel("372915", "Hotel Marbella", "3", "5605", "url2");
    private final Hotel villavicencio = new Hotel("372916", "Hotel Ol Castilla Villavicencio", "3", "87945", "url13");
    private final Hotel trem = new Hotel("372918", "Pousada Estação do Trem", "3", "6195", "url14");
    private final Hotel cochera = new Hotel("372919", "Hotel y Cabañas La Cochera", "0", "394", "url15");
    private final Hotel miramar = new Hotel("372920", "Club Maeva Miramar", "3", "8965", "url16");
    private final Hotel poisada = new Hotel("372921", "Trilha do Mar Pousada", "3", "8965", "url11");

    @Before
    @After
    public void setUp() {
        operations.execute((RedisConnection connection) -> {
            connection.flushDb();
            return "OK";
        });
    }

    @Test
    public void saveSingleEntity() {

        repository.save(praia);

        assertThat(operations.execute(
                (RedisConnection connection) -> connection.exists(("hotel:" + praia.getHotelId()).getBytes(CHARSET))),
                is(true));
    }

    @Test
    public void findBySingleProperty() {

        flushTestHotels();

        List<Hotel> starks = repository.findByName(praia.getName());

        Assert.assertTrue(starks.get(0).getHotelId().equals(praia.getHotelId()));
    }

    @Test
    public void findAll() {

        flushTestHotels();

        Iterable<Hotel> aryaStark = repository.findAll();

        assertThat(aryaStark, hasItem(trem));
    }

    private void flushTestHotels() {
        repository.save(Arrays.asList(praia, marbella, villavicencio, trem, cochera, miramar, poisada));
    }
}
