#**Dependencias y deploy**#

##Se utilizaron los siguientes módulos de Spring:
* spring-boot-starter-web
* spring-web
* spring-boot-starter-web
* spring-boot-starter-data-redis
* spring-boot-starter-aop
* spring-boot-starter-test
* spring-retry

##Otras dependencias:
* commons-lang3
* org.apache.httpcomponents
* embedded-redis

##Se utilizo el proyecto Lombok
https://projectlombok.org/
**Se debe instalar en el ide en caso de querer levantar el proyecto.**

## Base de datos ##
Se utilizo Redis como base de datos en memoria. Se debe tener instalado y iniciado el servicio para poder probar la App.
Por defecto se encuentra configurado todo en el application.properties:
    redis.port=6379
    redis.host=localhost

## Pasos para levantar el proyecto:
1. descomprimir el proyecto.
2. entrar en la carpeta del proyecto.
3. levantar el servidor de redis.
4. mvn spring-boot:run

Cuando levanta la app se crean datos de ejemplos según la documentación. Se trae un snashopt a memoria.

#Request#

* http://localhost:1313/destinder/hotels/prices?city_id=5605 -> **GET**.
* http://localhost:1313/destinder/hotels/likes -> **POST** BODY: {"hotel_id":"372971"}
* http://localhost:1313/destinder/hotels/likes -> **DELETE** BODY: {"hotel_id":"372971"}

El punto 5 no se logra entender que se quiere.
Los test estan solo los de la base de datos. 